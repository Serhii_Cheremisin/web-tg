const Product = ({id, name, description, price, onClick}) => {
    return <div className={'productCart'}>
        <p>{name}</p>
        <p>{description}</p>
        <p>{price}</p>
        <button onClick={()=>onClick(id)} className={'button'}>Buy</button>
    </div>
}

export { Product }
