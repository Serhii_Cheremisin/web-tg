import {useCallback, useEffect, useState} from "react";
import { Product } from "../product";
import './style.css'
const tg = window.Telegram.WebApp;

const products = [
    {
        id: '1',
        name: 'Product 1',
        description: 'Description of product 1',
        price: 100
    },
    {
        id: '2',
        name: 'Product 2',
        description: 'Description of product 2',
        price: 120
    },
    {
        id: '3',
        name: 'Product 3',
        description: 'Description of product 3',
        price: 130
    },
    {
        id: '4',
        name: 'Product 4',
        description: 'Description of product 4',
        price: 140
    }
]

const Table = () => {
    const [boughtProducts, setBoughtProducts] = useState([])
    const onSendData = useCallback(() => {
        const data = boughtProducts
        tg.sendData(JSON.stringify(data))
    }, [boughtProducts])

    useEffect(() => {
        tg.onEvent('mainButtonClicked', onSendData)
        return () => {
            tg.offEvent('mainButtonClicked', onSendData)
        }
    }, [onSendData])

    const addItem = (id) => {
        const result = products.filter(item => id === item.id)
        setBoughtProducts([...boughtProducts, ...result])
    }

    const onToggleButton = () => {
        if(tg.MainButton.isVisible) {
            tg.MainButton.hide()
        } else {
            tg.MainButton.show()
        }
    }

    return <div>
        <div className='table'>
        {products.map((item) => <Product key={item.id} {...item} onClick={addItem} />)}
    </div>
        <button onClick={onToggleButton}>Main button</button>
        <button onClick={()=>setBoughtProducts([])} className={'button'}>Clear</button>
    </div>
}

// in tg send boughtProducts

export { Table }
